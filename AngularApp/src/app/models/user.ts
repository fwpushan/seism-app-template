export interface User {
    email: string;
    id: string;
    token: string;
    profileImageURL: string;
    name: string;
}
