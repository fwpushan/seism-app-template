import { Component, OnInit } from '@angular/core';
import { UtilityService} from '../../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isLogin = false;
  constructor() { }

  ngOnInit() {
    this.isLogin = UtilityService.isUserLogin;
  }

}
