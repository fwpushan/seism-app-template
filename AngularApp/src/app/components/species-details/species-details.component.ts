import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-species-details',
  templateUrl: './species-details.component.html',
  styleUrls: ['./species-details.component.css']
})
export class SpeciesDetailsComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private zone: NgZone) {
    this.zone.run(() => {
      this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        latin: ['', Validators.required],
        category: ['', Validators.required],
        description: ['', [Validators.required]]
      });
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      console.log(`SpeciesDetailsComponent: Action: ${params.action}`);
      console.log(`SpeciesDetailsComponent: Action: ${params.id}`);
    });
  }

}
