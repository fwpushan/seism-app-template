
declare const window: any;

class RemoteEndPoints {

    private static instance: RemoteEndPoints;

    public static getInstance() {
        return this.instance || (this.instance = new this());
    }

    get baseURL(): string {
        return window.location.origin || `http://localhost`;
    }

    get categories(): string {
        return this.baseURL + `/api/categories`;
    }
}

export const RemoteEndPointService = RemoteEndPoints.getInstance();
