export enum AppRoutes {
    Root = '',
    Detail = 'details/:action/:id',
    DetailRef = 'details'
}

export enum AppRoutesParams {
    DetailAdd = 'add',
    DetailEdit = 'edit',
    DetailView = 'view'
}
