import * as moment from 'moment';
import {User} from '../models';

import * as Cookies from 'js-cookie';

declare const document: any;
const UserCookieName = 'user';

export class StoreService {
    // Share Instance
    private static instance: StoreService;

    // Local var
    user: User = null;

    // Get Share Instance
    public static getInstance() {
        return this.instance || (this.instance = new this());
    }


    // Saving user in cookies
    public saveUser(user: User) {
        const userstring = `${JSON.stringify(user)}`;
        Cookies.set(UserCookieName, userstring, { expires: 5, path: '' });
    }

    // Loading user from cookies
    public loadUser(): User {
        if (this.user) {
            return this.user;
        }
        const usrstring = Cookies.get(UserCookieName);
        if (usrstring) {
            console.log(`User is logged in`);
            try {
                const usr: User = JSON.parse(usrstring) as User;
                console.dir(usr);
                this.user = usr;
                return usr;
            } catch (error) {
                console.log(`User string parsing error: ${error}`);
                return null;
            }
        } else {
            console.log(`User is not loggedin`);
        }
    }

    // Loggingout user and removing it from cookies
    public logout() {
        Cookies.remove(UserCookieName);
        this.user = null;
    }


    public get isUserLoggedIn() {
        return this.user ? true : false;
    }
}

export const Store = StoreService.getInstance();
