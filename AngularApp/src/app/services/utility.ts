import { Store } from './store-service';
import { AppRoutes} from '../constants/app-routes.enum'


export class Utility {
   // Share Instance
   private static instance: Utility;


   // Get Share Instance
   public static getInstance() {
       return this.instance || (this.instance = new this());
   }

   // Bootstrap Login check
   public get isUserLogin(): boolean {
       Store.loadUser();
       return Store.isUserLoggedIn;
   }

   public appRoute(route: AppRoutes): string {
       return `/${route}`;
   }
}

export const UtilityService = Utility.getInstance();
