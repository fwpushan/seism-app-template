import { StoreService } from './store-service';
import {User} from '../models';

describe('StoreService', () => {
  it('should create an instance', () => {
    expect(new StoreService()).toBeTruthy();
  });

  it('should save user in cookie and load it', () => {
    const store = StoreService.getInstance();
    expect(store).toBeTruthy();

    // Saving user
    const user: User = {
      name: `FWPushan`,
      email: `pushan@gmail.com`,
      token: `12AbAAcx`,
      profileImageURL: `img`,
      id: `1029384756`
    };
    store.saveUser(user);

    // Loading user
    const result: User = store.loadUser();
    expect(result).toBeTruthy();
    expect(result.email).toEqual(user.email);
    expect(result.name).toEqual(user.name);
    expect(result.id).toEqual(user.id);
    expect(store.isUserLoggedIn).toEqual(true);
    expect(store.user).toBeTruthy();

    // Logout
    store.logout();
    expect(store.isUserLoggedIn).toEqual(false);
    expect(store.user).toBeNull();

    // Checking load user after logout
    store.loadUser();
    expect(store.isUserLoggedIn).toEqual(false);
    expect(store.user).toBeNull();
  });

});
