## Simple Template for Angular, Flask API and PostgreSQL database with Docker-compose and nginx

## About
This application consists of:
* A basic web form built using React
* A Flask API 
* nginx reverse proxy
* Docker-compose
* Angular Template App

## Prerequisites
To run the full application you will need to:
1. Clone the repo
2. Install Docker and Docker Compose

## Running the application (macOS, Linux, Unix)
At the project root, run:  
  
sh build.sh [-c || --clean to clean ] (to build and run )  
sh setup_api.sh (setting up api)  

## Running the application windows 
At the project root, run following  
  
docker-compose build  
docker-compose -f docker-compose.yml run api python manage.py recreate-db  
docker-compose -f docker-compose.yml run api python manage.py seed-db  
docker-compose up -d  


The application will be available at http://localhost/

API endpoints will be available at:
* http://localhost/api/food/categories
* http://localhost/api/food/types

Submissions, locations and new food types are not visible in the API, but you can see what’s happening in the database with:

1. `docker-compose -f docker-compose.yml exec db psql -U postgres`
2. `\c db_dev`

To run the Angular Client alone, you will need to:
1. `cd AngularApp`
2. Install the dependencies by running `npm install`
3. Start the server with `npm start`

## Running tests

To run tests for the Flask API:
1. `docker-compose -f docker-compose.yml run form-handler python manage.py test`

## Killing the application
sh stop.sh 
or 
docker-compose down -v (windows and others)