from flask import Blueprint, jsonify, request
from sqlalchemy import exc
from main import db
from main.api.models import User

users_blueprint = Blueprint('users', __name__)

# POST user, registering their information
@users_blueprint.route('/api/v1/users', methods=['POST'])
def create_user():
    # Default error response
    response_object = {
        'errors': ['Invalid payload.']
    }
    post_data = request.get_json()
    if not post_data:
        return jsonify(response_object), 400

    # Get request data
    google_id = post_data.get('gid')
    username = post_data.get('username')
    email = post_data.get('email')
    access_token = post_data.get('access_token')

    # Add to DB
    user = User(gid=google_id, username=username, email=email, access_token=access_token)
    db.session.add(user)
    db.session.commit()

    # Return success and created object
    response_object = {
        'status': 'Successfully created user.',
        'user': user.to_json()
    }
    return jsonify(response_object), 201