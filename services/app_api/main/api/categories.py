from flask import Blueprint, jsonify
from main.api.models import Category

categories_blueprint = Blueprint('categories', __name__)

# GET category table contents
@categories_blueprint.route('/api/v1/categories', methods=['GET'])
def get_categories():
    response_object = {
        'categories': [category.to_json() for category in Category.query.all()]
    }
    return jsonify(response_object), 200